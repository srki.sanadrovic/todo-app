import { Todo } from '../types/Types'

export function getDoneTodosNumber(todos: Todo[]): number {
	const doneTodos = todos.filter(todo => todo.isCompleted)

	return doneTodos.length
}
