export interface Todo {
	label: string
	date: string
	isCompleted: boolean
}
