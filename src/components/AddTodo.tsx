import React, { SyntheticEvent, useState } from 'react'
import { Todo } from '../types/Types'

interface Props {
	addTodo: (newTodo: Todo) => void
	closeModal: () => void
}

function AddTodo({ addTodo, closeModal }: Props) {
	const [label, setLabel] = useState('')
	const [date, setDate] = useState('')

	function handleLabelChange(e: React.FormEvent<HTMLInputElement>) {
		setLabel(e.currentTarget.value)
	}

	function handleDateChange(e: React.FormEvent<HTMLInputElement>) {
		const newDate = e.currentTarget.value
		setDate(newDate)
	}

	function handleSubmit() {
		if (label && date) {
			const newTodo: Todo = {
				label: label,
				date: date,
				isCompleted: false,
			}

			addTodo(newTodo)
			closeModal()
		}
	}

	return (
		<div className='add-todo'>
			<div className='add-todo-form'>
				<div>
					<button onClick={closeModal}>X</button>
				</div>

				<input
					type='text'
					value={label}
					onChange={handleLabelChange}
					placeholder='Enter task label'
				/>
				<input type='date' value={date} onChange={handleDateChange} />

				<button onClick={handleSubmit}>Add</button>
			</div>
		</div>
	)
}

export default AddTodo
