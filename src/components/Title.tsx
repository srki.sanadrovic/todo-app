interface Props {
	totalTasks: number
	tasksDone: number
}

function Title({ totalTasks, tasksDone }: Props) {
	return (
		<div className='two-col'>
			<div className='left-col'></div>
			<div className='right-col title'>
				<h1>My Tasks</h1>
				<span>
					{tasksDone} of {totalTasks} tasks
				</span>
			</div>
		</div>
	)
}

export default Title
