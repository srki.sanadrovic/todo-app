import { Todo } from '../types/Types'
import TodoItem from './TodoItem'

interface Props {
	todos: Todo[]
	completeTodo: (label: string) => void
	addTodo: (newTodo: Todo) => void
}

function TodosList({ todos, completeTodo, addTodo }: Props) {
	return (
		<div>
			{todos.map(todo => (
				<TodoItem todo={todo} completeTodo={completeTodo} key={todo.label} />
			))}
		</div>
	)
}

export default TodosList
