import { Todo } from '../types/Types'

interface Props {
	todo: Todo
	completeTodo: (label: string) => void
}

function TodoItem({ todo, completeTodo }: Props) {
	return (
		<label className='two-col todo'>
			<div className='left-col todo-check'>
				<input
					type='checkbox'
					checked={todo.isCompleted}
					onChange={() => completeTodo(todo.label)}
				/>
			</div>
			<div className='right-col todo-label'>
				<span>{todo.label}</span>
				<span>{todo.date}</span>
			</div>
		</label>
	)
}

export default TodoItem
