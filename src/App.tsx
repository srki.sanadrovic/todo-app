import { SyntheticEvent, useEffect, useState } from 'react'
import axios from 'axios'
import { Todo } from './types/Types'
import Title from './components/Title'
import { getDoneTodosNumber } from './helpers/Helpers'
import TodosList from './components/TodosList'
import AddTodo from './components/AddTodo'

function App() {
	const [todos, setTodos] = useState<Todo[]>([])
	const [modalIsOpen, setModalIsOpen] = useState(false)

	useEffect(() => {
		async function fetchData() {
			const result = await axios.get('data.json')
			setTodos(result.data.todos)
		}

		fetchData()
	}, [])

	function completeTodo(label: string): void {
		setTodos(prevState =>
			prevState.map(todo =>
				todo.label === label ? { ...todo, isCompleted: !todo.isCompleted } : todo
			)
		)
	}

	function addTodo(newTodo: Todo): void {
		setTodos(prevState => [...prevState, newTodo])
	}

	function handleModal() {
		setModalIsOpen(prevState => !prevState)
	}

	return (
		<div className='App'>
			<Title totalTasks={todos.length} tasksDone={getDoneTodosNumber(todos)} />
			<TodosList todos={todos} completeTodo={completeTodo} addTodo={addTodo} />

			<button className='add-button' onClick={handleModal}>
				<span>+</span>
			</button>

			{modalIsOpen && <AddTodo addTodo={addTodo} closeModal={handleModal} />}
		</div>
	)
}

export default App
